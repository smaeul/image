====================
 Adélie Image Tools
====================
:Author:
  A\. Wilcox
:Version:
  1.0
:Status:
  Production
:Copyright:
  © 2018 Adélie Linux Team.  NCSA open source licence.



Introduction
============

This repository contains the Adélie Linux image tools, which are used by the
Release Engineering team to create the official Adélie Linux ISO images.

License
```````
The tools contained in this repository are licensed under the NCSA open source
license.  The bzImage files are created from the ``system/easy-kernel``
package (the ``/boot/vmlinuz`` file created for each architecture is copied
unchanged to ``bzImage-${ARCH}``), and as such are licensed under the kernel's
GPL2 license.

Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Usage
=====

This section contains usage information for the software in this repository.


``adelie-build-cd``
```````````````````

This script is where the magic happens.  ``adelie-build-cd`` will create the ISO for
the architecture specified (defaulting to the currently running system
architecture if not specified).

``adelie-build-cd`` requires the following utilities to be installed on your computer:

* apk-tools
* cpio
* squashfs-tools
* A variant of ``mkisofs`` - releng uses the cdrkit package.

``adelie-build-cd`` is run on the Z shell on official releng machines, but should be
functional in any POSIX compliant shell; it relies on no non-POSIX shell
constructs.

The ``cdinit-${ARCH}`` file must exist in the directory that you run
``adelie-build-cd``; it does not need to be in the same directory as the ``adelie-build-cd``
script itself.


``cdinit``
``````````

This functions as early init for the CD, probing for the SquashFS root and
mounting it appropriately.  Pre-compiled binaries statically linked to the
current release's version of musl, util-linux, and eudev are provided for each
Tier I and Tier II architecture of the current release.

You may build your own ``cdinit``, for verification purposes or for another
architecture, and place it in the same directory as you invoke ``adelie-build-cd``.



Reporting Issues
================

If you have issues installing with the official media, you should report your
issue to `the BTS`_.

If you have issues running ``adelie-build-cd`` or compiling ``cdinit``, you may file
an issue in the GitLab repository.

.. _the BTS: http://bts.adelielinux.org/
