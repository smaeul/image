mkdir -p cdroot-ppc/boot

if ! type grub-mkimage>/dev/null; then
	echo "GRUB image cannot be created.  Using stale copy."
	curl "https://distfiles.adelielinux.org/adelie/1.0-alpha/ppc/grubcore.img" > cdroot-ppc/boot/grubcore.img
else
	grub-mkimage -c ppc/early.cfg -v -p boot -o cdroot-ppc/boot/grubcore.img -O powerpc-ieee1275 boot btrfs datetime disk ext2 gfxmenu help hfs hfsplus ieee1275_fb iso9660 jfs ls luks lvm macbless macho nilfs2 ofnet part_apple part_gpt part_msdos png scsi search xfs linux reboot gfxterm gfxterm_background gfxterm_menu
fi

cp AdelieTux.icns 'cdroot-ppc/Icon'
cp ppc/grub.cfg cdroot-ppc/boot/grub.cfg
cp ppc/ofboot.b cdroot-ppc/boot/ofboot.b
cp cdroot-ppc/boot/ofboot.b cdroot-ppc/boot/bootinfo.txt

# CHRP
mkdir -p cdroot-ppc/ppc
cp cdroot-ppc/boot/bootinfo.txt cdroot-ppc/ppc/bootinfo.txt
