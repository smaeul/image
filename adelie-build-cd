#!/bin/sh -e

def_arch=$(uname -m)
def_ver="1.0-beta4"
declare -r PROGNAME=$(basename $0)


warn() {
    printf '>>> '
    printf '\033[01;33mWARNING\033[00;39m '
    printf '>>> '
    printf '\033[01;33mWARNING\033[00;39m '
    printf '>>> '
    printf '\033[01;33mWARNING\033[00;39m '
    printf '>>> '
    printf '\033[01;33mWARNING\033[00;39m '
    printf '>>>\n'
}


fatal() {
    printf '>>> '
    printf '\033[01;31mERROR\033[00;39m '
    printf '>>> '
    printf '\033[01;31mERROR\033[00;39m '
    printf '>>> '
    printf '\033[01;31mERROR\033[00;39m '
    printf '>>> '
    printf '\033[01;31mERROR\033[00;39m '
    printf '>>>\n'
}


ensure_commands() {
    if ! type apk>/dev/null 2>/dev/null; then
        fatal
        printf 'You must have apk installed.  On Gentoo, see sys-devel/apk-tools.\n'
        exit -1
    fi

    if ! type cpio>/dev/null 2>/dev/null; then
        fatal
        printf 'You must have cpio installed.  On Gentoo, see app-arch/cpio.\n'
        exit -1
    fi

    if ! type mksquashfs>/dev/null 2>/dev/null; then
        fatal
        printf 'You must have mksquashfs installed.  On Gentoo, see sys-fs/squashfs-tools.\n'
        exit -1
    fi

    if ! type xorriso>/dev/null 2>/dev/null; then
        fatal
        printf 'You must have xorriso installed:\n\n'
	printf '* cdrkit (Alpine, Gentoo) will not generate a usable PPC ISO.\n'
	printf '* wodim (Debian) will not generate a usable PPC64 ISO.\n'
	printf '* cdrtools (Schily) will overrun the PPC64 ISO and write junk to grubcore.img.\n'
	printf '\nSorry, but xorriso really is required.\n'
        exit -1
    fi
}


usage() {
    printf 'usage: %s [-a ARCH] [-f|--full] [-p|--phase PHASE] [-v VERSION] [--help]\n\n' $PROGNAME
    printf 'Create an Adélie Linux CD image (.ISO) using the specified parameters.\n\n'
    printf 'Default ARCH: %s\n' $def_arch
    printf 'Default VERSION: %s\n' $def_ver
    printf 'Valid phases: clean install initrd iso all\n'
}


while [ -n "$1" ]; do
    case $1 in
        -a | --arch)
            shift
            declare -r MY_ARCH=$1
            ;;
        -h | --help)
            usage
            exit
            ;;
        -f | --full)
            declare -r DO_FULL=full
            ;;
        -p | --phase)
            shift
            declare -r MY_PHASE=$1
            ;;
        -s | --sign)
            declare -r SIGN=yes
            ;;
        -v | --version)
            shift
            declare -r MY_VER=$1
            ;;
        *)
            usage >&2
            exit -1
            ;;
    esac
    shift
done

set -a
declare -r ARCH=${MY_ARCH:-$def_arch}
declare -r LDARCH=${LDARCH:-$ARCH}
declare -r PHASE=${MY_PHASE:-all}
declare -r VERSION=${MY_VER:-$def_ver}
declare -r URL=${MY_URL:-https://distfiles.adelielinux.org/adelie/$VERSION/}
set +a

ensure_commands

header() {
	printf '\033[01;32m * \033[37m%s\033[00;39m\n' "$1"
}

header 'Adélie Linux CD Creation Tool'
printf '\n'

clean_dirs() {
	warn
	printf 'This will erase all files at the directories %s/cdroot-%s\n' `pwd` $ARCH
	printf '%s/initrd-%s and %s/squashroot-%s.\n\n' `pwd` $ARCH `pwd` $ARCH
	printf 'When you are ready, press RETURN.  To cancel, press Ctrl-C.\n'
	read

	rm -rf cdroot-$ARCH
	rm -rf initrd-$ARCH
	rm -rf squashroot-$ARCH
	mkdir cdroot-$ARCH
	mkdir initrd-$ARCH
	mkdir squashroot-$ARCH
}

install_pkgs() {
	header "Installing base system to squash root..."

	declare -r PACKAGES=$(cat packages 2>/dev/null || fatal 'No core packages specified')
	declare -r ARCH_PKGS=$(cat packages-$ARCH 2>/dev/null || echo '')

	mkdir -p squashroot-$ARCH/etc/apk/keys
	cp 'packages@adelielinux.org.pub' squashroot-$ARCH/etc/apk/keys/
	# XXX: Handle pre-install scripts.
	mkdir -p squashroot-$ARCH/dev
	mknod squashroot-$ARCH/dev/urandom c 1 9
	mkdir -p squashroot-$ARCH/usr/sbin
	cp addgroup adduser squashroot-$ARCH/usr/sbin/
	apk --arch $ARCH \
		-X "$URL/system/$EXTRA_MIRROR" \
		-X "$URL/user/$EXTRA_MIRROR" \
		-U --root squashroot-$ARCH --initdb add $PACKAGES $ARCH_PKGS
}

make_structure() {
	mkdir -p squashroot-$ARCH/home/live
	cp squashroot-$ARCH/etc/skel/.zshrc squashroot-$ARCH/home/live/
	chown 1000:1000 squashroot-$ARCH/home/live/.zshrc
	mkdir squashroot-$ARCH/target
	mkdir -p squashroot-$ARCH/media/live
	mkdir -p squashroot-$ARCH/etc/runlevels/{sysinit,boot,default,shutdown}

	echo 'adelie-live' > squashroot-$ARCH/etc/hostname
	echo 'mtab_is_file=no' > squashroot-$ARCH/etc/conf.d/mtab

	for siservice in udev udev-trigger lvmetad; do
		ln -s /etc/init.d/$siservice \
		   squashroot-$ARCH/etc/runlevels/sysinit/$siservice
	done

	for bootservice in root binfmt bootmisc fsck hostname hwclock keymaps \
	    localmount loopback modules mtab procfs sysctl sysfsconf termencoding \
	    tmpfiles.setup urandom; do
		ln -s /etc/init.d/$bootservice \
		   squashroot-$ARCH/etc/runlevels/boot/$bootservice
	done

	cat >squashroot-$ARCH/etc/fstab <<- FSTAB
		# Welcome to Adélie Linux.
		# This fstab(5) is for the live media only.  Do not edit or use for your installation.

		tmpfs	/tmp		tmpfs	defaults	0	1
		proc	/proc		proc	defaults	0	1
	FSTAB

	sed -i 's#/root:/bin/sh$#/root:/bin/zsh#' squashroot-$ARCH/etc/passwd
	echo 'live:x:1000:1000:Live User:/home/live:/bin/zsh' >> squashroot-$ARCH/etc/passwd
	echo 'live:x:1000:' >> squashroot-$ARCH/etc/group
	echo 'live::::::::' >> squashroot-$ARCH/etc/shadow
	chown 1000:1000 squashroot-$ARCH/home/live

	sed -i 's/pam_unix.so$/pam_unix.so nullok_secure/' squashroot-$ARCH/etc/pam.d/base-auth

	cat >squashroot-$ARCH/etc/resolv.conf <<- RESOLVE
		nameserver 2620:fe::fe
		nameserver 9.9.9.9
		nameserver 149.112.112.112
	RESOLVE

	cat >squashroot-$ARCH/etc/apk/repositories <<-REPOS
		https://distfiles.adelielinux.org/adelie/$VERSION/system/$EXTRA_MIRROR
		https://distfiles.adelielinux.org/adelie/$VERSION/user/$EXTRA_MIRROR
	REPOS
	# Saves first-sync on the media if a package is required.
	apk --root squashroot-$ARCH update

	cat >squashroot-$ARCH/etc/issue <<-ISSUE
		Welcome to Adélie Linux!
		You may log in as 'root' to install, or 'live' to play around.

		Have fun.

	ISSUE

	if test -n "${DO_FULL+full}"; then
		# This ensures we have documentation available on-disc, and to install.
		apk --arch $ARCH \
			-X "$URL/system/$EXTRA_MIRROR" \
			-X "$URL/user/$EXTRA_MIRROR" \
			--root squashroot-$ARCH add docs
		declare -r PACKAGES_DIR=squashroot-$ARCH/packages/$ARCH
		mkdir -p $PACKAGES_DIR
		# Fetch all APKs.
		apk --arch $ARCH \
			-X "$URL/system/$EXTRA_MIRROR" \
			-X "$URL/user/$EXTRA_MIRROR" \
			--root squashroot-$ARCH fetch -o $PACKAGES_DIR $(apk --root squashroot-$ARCH info)
		# Sign the index using the signing key used by abuild.
		if test -n "${SIGN+doit}"; then
			apk index --description "$VERSION/$ARCH Live CD" -o .tmp.APKINDEX.tar.gz $PACKAGES_DIR/*.apk
			abuild-sign -q .tmp.APKINDEX.tar.gz
			mv .tmp.APKINDEX.tar.gz $PACKAGES_DIR/APKINDEX.tar.gz
		fi
	fi

	cp AdelieTux.icns cdroot-$ARCH/.VolumeIcon.icns
}

squash_it() {
	header 'Creating compressed file system image...'

	mksquashfs squashroot-$ARCH cdroot-$ARCH/adelie.squashfs -noappend
}

make_initrd() {
	header 'Creating initrd structure...'

	# mount points
	mkdir initrd-$ARCH/dev
	mkdir initrd-$ARCH/media
	mkdir initrd-$ARCH/{newroot,lowerroot,upperroot}
	chmod 755 initrd-$ARCH/{newroot,lowerroot,upperroot}
	mkdir initrd-$ARCH/proc
	mkdir initrd-$ARCH/sys

	# manual /dev nodes for initial udev startup
	mknod -m 600 initrd-$ARCH/dev/console c 5 1
	mknod -m 666 initrd-$ARCH/dev/null c 1 3
	mknod -m 666 initrd-$ARCH/dev/ptmx c 5 2
	mknod -m 666 initrd-$ARCH/dev/random c 1 8
	mknod -m 666 initrd-$ARCH/dev/tty c 5 0
	mknod -m 620 initrd-$ARCH/dev/tty1 c 4 1
	mknod -m 666 initrd-$ARCH/dev/urandom c 1 9
	mknod -m 666 initrd-$ARCH/dev/zero c 1 5

	# base
	mkdir initrd-$ARCH/lib
	cp squashroot-$ARCH/lib/ld-musl-$LDARCH.so.1 initrd-$ARCH/lib/
	cp squashroot-$ARCH/lib/libblkid.so.1 initrd-$ARCH/lib/
	cp squashroot-$ARCH/lib/libuuid.so.1 initrd-$ARCH/lib/

	# udev
	mkdir -p initrd-$ARCH/etc/udev
	mkdir initrd-$ARCH/run
	mkdir initrd-$ARCH/sbin
	cp squashroot-$ARCH/bin/udevadm initrd-$ARCH/sbin/
	cp squashroot-$ARCH/sbin/udevd initrd-$ARCH/sbin/
	cp squashroot-$ARCH/lib/libkmod.so.2 initrd-$ARCH/lib/
	cp squashroot-$ARCH/lib/liblzma.so.5 initrd-$ARCH/lib/
	cp squashroot-$ARCH/lib/libudev.so.1 initrd-$ARCH/lib/
	cp squashroot-$ARCH/lib/libz.so.1 initrd-$ARCH/lib/

	# init
	cp cdinit-$ARCH initrd-$ARCH/init

	header 'Compressing initrd...'

	pushd initrd-$ARCH
	find . | cpio -H newc -o > ../cdroot-$ARCH/initrd
	popd
	gzip -9 cdroot-$ARCH/initrd
	mv cdroot-$ARCH/initrd.gz cdroot-$ARCH/initrd
}

prepare_cdroot() {
	if test -f post-$ARCH.sh; then
		header 'Running architecture-specific scripts...'
		sh post-$ARCH.sh
	fi

	header 'Adding kernel...'

	mv squashroot-$ARCH/boot/vmlinu* cdroot-$ARCH/bzImage
}

create_iso() {
	local CD_VERSION="$VERSION"
	header 'Creating the CD...'

	declare -r CD_PARAMS=$(cat iso-params-$ARCH)
	CD_VERSION=${CD_VERSION/-alpha/a}
	CD_VERSION=${CD_VERSION/-beta/b}
	CD_VERSION=${CD_VERSION/-rc/rc}
	mkdir -p out
	xorriso -as mkisofs -o out/adelie-${DO_FULL:-live}-$ARCH-$VERSION-$(date +%Y%m%d).iso -joliet -rational-rock -V "Adelie $CD_VERSION $ARCH" ${CD_PARAMS} cdroot-$ARCH
}

case $PHASE in
	clean)
		clean_dirs
		;;
	install)
		install_pkgs
		make_structure
		;;
	initrd)
		make_initrd
		;;
	iso)
		create_iso
		;;
	all)
		clean_dirs
		install_pkgs
		make_structure
		make_initrd
		squash_it
		prepare_cdroot
		create_iso
		;;
	all_but_iso)
		clean_dirs
		install_pkgs
		make_structure
		make_initrd
		squash_it
		prepare_cdroot
		;;
	*)
		fatal
		printf 'Unknown phase %s.  Stop.\n' $PHASE
		;;
esac
